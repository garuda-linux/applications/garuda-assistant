#include "garudaassistant.h"

#include <QApplication>

// #include <QTranslator>
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QTranslator myappTranslator;
    if (!myappTranslator.load("garudaassistant_" + QLocale::system().name(), "/usr/share/garuda-assistant/translations")) {
        QTextStream(stdout) << "Warning: No translations available" << Qt::endl;
    }
    a.installTranslator(&myappTranslator);
    GarudaAssistant w;
    if (!w.setup())
        return 0;
    w.show();
    return a.exec();
}
